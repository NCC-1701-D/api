<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $item1 = (new Item())
            ->setName('Produkt 1')
            ->setAmount(4);
        $item2 = (new Item())
            ->setName('Produkt 2')
            ->setAmount(12);
        $item3 = (new Item())
            ->setName('Produkt 5')
            ->setAmount(0);
        $item4 = (new Item())
            ->setName('Produkt 7')
            ->setAmount(6);
        $item5 = (new Item())
            ->setName('Produkt 8')
            ->setAmount(2);

        $manager->persist($item1);
        $manager->persist($item2);
        $manager->persist($item3);
        $manager->persist($item4);
        $manager->persist($item5);

        $manager->flush();
    }
}
