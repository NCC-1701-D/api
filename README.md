# API

## Development

### Setting up dev environment

1. Set up environment variables:

        cp .env.dist .env

2. Run docker containers:

        docker-compose up -d --build
    
3. Log into api container:

        docker exec -it api /bin/bash

4. Install dependencies(in container):

        composer install
    
5. Optionally create database if not exists(in container):
    
        php bin/console doctrine:database:create
    
6. Run migrations(in container):

        php bin/console doctrine:migrations:migrate
    
7. Load fixtures(in container):

        php bin/console doctrine:fixtures:load

8. Clear cache(in container):

        php bin/console cache:clear
        